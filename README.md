# code-en-bois

Ce dépôt stocke les fichiers publics relatifs aux kits Code en Bois et permet d'accéder aux versions successives des documents. 

CODE EN BOIS est un système innovant et écologique qui permet d’initier à la programmation en manipulant des briques d’instructions en bois. Plus d'informations sur le site web:
https://codeenbois.fr/

## Licence d'utilisation

Le carnet de défis ainsi que livret explicatif sont licence CREATIVE COMMONS BY-SA
https://creativecommons.org/licenses/by-sa/2.0/fr/
Attribution - Partage dans les Mêmes Conditions 2.0 France (CC BY-SA 2.0 FR), voir les détails sur le lien. 

## Tutoriels

Des vidéos tutoriels sont disponibles pour mieux comprendre le fonctionnement de l'outil ainsi que pour s'auto-former en tant qu'animateur d'ateliers. Les liens:
MOOC formation animateur:
https://www.youtube.com/playlist?list=PLpwqWGN7OhyI8z5CuZIiX17r9kJ_9nJOY
Mini MOOC préparation de l'atelier:
https://www.youtube.com/watch?v=R8xErzvJpW8&list=PLpwqWGN7OhyJU4VHKP8kwwLTZMNNt9C9T

## C'est quoi Gitlab?

Gitlab est une plateforme largement utilisée par les développeurs du monde entier pour stocker, versionner et automatiser le déploiement de leurs codes. 

